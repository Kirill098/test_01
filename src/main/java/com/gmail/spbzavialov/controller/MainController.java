package com.gmail.spbzavialov.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Configuration
@Controller
public class MainController {

    @Bean
    public MyBean myBean() {
        return new MyBean();
    }

    private static Log log = LogFactory.getLog(MainController.class);

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView main() {

        log.info("Create view ...");

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");

        ConfigurableApplicationContext context =
                new AnnotationConfigApplicationContext(MainController.class);
        MyBean bean = context.getBean(MyBean.class);
        bean.doSomething();

        log.info("Return view ...");

        return modelAndView;
    }
}
